import React from "react";

import "./FooterComp.css";

const Footer = (props) => {
  return (
    <div className="bg-footer spacing">
      <p className="text-center footer-text">
        Copyright (c) 2020 Dream Holding
      </p>
    </div>
  );
};
export default Footer;
