import React, { useState, useEffect } from "react";
import { } from "react-bootstrap";
import { axiosBaseType } from "../../fetch/AxiosBase";
import * as signalr from "@aspnet/signalr";

// import axios from "axios";

import "./ChatComponent.css";
import SenderDiv from "./SenderDiv/SenderDiv";
import ReceiveDiv from "./ReceiveDiv/ReceiveDiv";

const axiosBase = axiosBaseType('chat');
const connect = new signalr.HubConnectionBuilder()
  .withUrl("https://buttchatloginservice.azurewebsites.net/chat")
  .build();

const ChatEndpoint = "/dialogflow";
export default function (props) {
  const [input, setInput] = useState("");
  const [msgs, setMsg] = useState([]);
  const [userName, setUserName] = useState("");

  const [isStart, setIsStart] = useState(false);

  const [logged, setLogged] = useState("");

  let textInp = React.createRef();
  let scrollDiv = React.createRef();

  const msgClassify = ({ type, message, userName, customData }) => {
    console.log(msgs);
    if (type === "receiver") {
      return <ReceiveDiv message={message} username={userName} />;
    } else if (type === "sender") {
      return <SenderDiv message={message} username={userName} customData={customData} />;
    }
  };



  const connectChat = () => {
    connect
      .start()
      .then(() => {
        setLogged("Connected to chat!");
        setIsStart(true);
        console.log("Connected!");
      })
      .catch((err) => console.error("SignalR Connection Error: ", err));

    connect.on("ReceiveMessage", (user, message) => {
      console.log("Rei: " + message);
      const receiveObj = { type: "sender", message, userName: user };
      setMsg((msgs) => [...msgs, receiveObj]);
    });
  };

  const sendMsg = (event) => {
    if (event.key === "Enter") {
      // connect.invoke("SendMessage", userName, input);
      const inputObj = { type: "receiver", message: input, userName: userName };
      setMsg((msgs) => [...msgs, inputObj]);
      const requestMsg = {
        chatContent: [input],
        sessionId: "123456789",
        user: "Nyan",
      };
      callAPI(requestMsg);
      setInput("");
    }
  };

  const handlePress = (e) => {
    if (e.key === "Enter") {
      connectChat();
    }
  };

  const callAPI = (message) => {
    axiosBase.post(`${ChatEndpoint}`, message)
      .then(response => {
        console.log(response.data.response);
        const theMessage = response.data.response;

        // setMsg(msg => [
        //   ...msg,theMessage
        // ])

        let res = {
          type: "sender",
          message: theMessage,
          customData: []
        };

        if (typeof theMessage === 'object') {
          res = {
            type: "sender",
            message: theMessage.message,
            customData: theMessage.customData
          }
          // if (theMessage.quick-res) {
          //   res = {
          //     type:"sender",
          //     message: theMessage.message,

          //   }
          // }
        }
        setMsg((msg) => [...msg, res]);
      });

  }
  //Scroll to bottom
  const scrollToEnd = () => {
    scrollDiv.current.scrollIntoView({ behavior: "smooth" });
  }
  //If get more msg run update 
  useEffect(() => {
    scrollToEnd();
  }, [msgs]);

  return (
    <div>
      {/* Hello world */}
      <div className="row rounded-lg overflow-hidden shadow">
        {/* Users box*/}
        {/* Chat Box*/}
        <div className="col-12 px-0">
          <div className="px-4 py-5 chat-box bg-white">
            {/* Sender Message*/}

            {msgs.length > 0 ? (
              msgs.map((msg, index) => (
                <div key={index}>{msgClassify(msg)}</div>
              ))
            ) : (
                <div></div>
              )}
            <div ref={scrollDiv}></div>
            {/* Typing area */}
          </div>

        </div>
      </div>
      <div className="input-group">
        <input
          type="text"
          placeholder="Type a message"
          aria-describedby="button-addon2"
          className="form-control rounded-0 border-0 py-4 bg-light"
          value={input}
          onChange={(event) => setInput(event.target.value)}
          onKeyPress={sendMsg}
          ref={textInp}
        />
      </div>
      <div>
        <input
          type="text"
          onChange={(event) => setUserName(event.target.value)}
          placeholder="Your name"
          onKeyPress={handlePress}
        />
       
      </div>
    </div>
  );
}
