import React from "react";
import './BookingTable.css';
import axios from "axios";
// import RepositoryAPI from "../../../fetch/RepositoryAPI";

// const BookingRepo = RepositoryAPI.get('booking');

const BookingTable = (props) => {
    const [userBooking, setUserBooking] = React.useState([]);
    //cmt
    // async function renderTableData() {
    //     const response = await BookingRepo.getBooking();
    //     console.log("Response: ");
    //     console.log(response.object);
    //     setUserBooking(userBooking => 
    //         [...userBooking, response.object]
    //     );
    //     console.log(userBooking);
    //     // const {username, bookingId, name, startTime, endTime, dateCreated, status} = userBooking;
        
    // };
    function getUserData() {
        axios.get('http://localhost:8099/bookings/')
            .then(response => {
                response.data.object.map(ele => {
                    setUserBooking(userBooking =>[
                        ...userBooking, ele
                    ])
                });
            });
    };

    React.useEffect(() => {
        getUserData();
    },[]);

    let tableContent = <p>Loading...</p>
    if (userBooking.length !== 0) {
        console.log(userBooking);
        tableContent = (
            userBooking.map((ele, index) => {
                return (<tr key={ele.username}>
                        <td>{ele.bookingId}</td>
                        <td>{ele.name}</td>
                        <td>{ele.startTime}</td>
                        <td>{ele.endTime}</td>
                        <td>{ele.dateCreated}</td>
                        <td>{ele.status}</td>
                    </tr>
                )
            })
        );
    } else {
        tableContent = <p>The list is empty...</p>
    };

    //component return
    return (
        <div>
            <h2 className="list-head" id='title'>This is your booking</h2>
            <table id='userbookings'>
                <thead>
                    <tr>
                        <th>BookingID</th>
                        <th>Name</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Date Created</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {tableContent}
                </tbody>
            </table>
        </div>
    )
}

export default BookingTable;