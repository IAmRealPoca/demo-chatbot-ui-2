import React from "react";
import "./LoginButton.css";

const LoginButton = () => {
    return (
        <div>
            <button className="bg-login nav-link" href="/login">
                Login
            </button>
        </div>
    );
};

export default LoginButton;