import React from 'react';
import { Card, Table, ButtonGroup, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faImage, faEdit, faTrash, faGenderless, faTransgender, faCreditCard,faPlus } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { Link ,Route} from 'react-router-dom';

class Perfume extends React.Component {
    constructor(pros) {
        super(pros);
        this.state = {
            perfumes: []
        };
    }
    componentDidMount() {
        this.findAllPerfume();
    }
    findAllPerfume() {
        axios.get("http://localhost:8686/perfume/")
            .then(res => res.data)
            .then((data) => {
                if (data === 1) {
                    data.gender = "Male";
                } else if (data === 2) {
                    data.gender = "Female";
                } else if (data === 3) {
                    data.gender = "Unisex";
                }
                console.log(data);

                this.setState({ perfumes: data });
            });
    }
    deletePerfume = (id) => {
        axios.delete("http://localhost:8686/perfume/delete/" + id)
            .then(res => {
                if (res.data != null) {
                    alert("Delete  succsess");
                    this.setState({ "show": true });
                    setTimeout(() => this.setState({ "show": false }), 3000);
                    this.setState({
                        perfumes: this.state.perfumes.filter(perfume => perfume.perfumeFragranticaId !== id)
                    });
                }
            });
    };

    render() {
        return (
            <Card className={"border text-white"}>
                <Card.Header className={"border border-dark bg-dark text-white"}><FontAwesomeIcon icon={faList} /> List Perfume </Card.Header>
                {/* <Link exact path='/perfume/add' component={Perfume}><FontAwesomeIcon icon={faPlus} /> Add Perfume</Link> */}
                
                <Link to="/perfume/add" className="nav-link"><FontAwesomeIcon icon={faCreditCard} />Add Perfume</Link>
                <Card.Body>
                    <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th>ID Perfume</th>
                                <th> Name</th>
                                <th><FontAwesomeIcon icon={faTransgender} /> Gender</th>
                                <th><FontAwesomeIcon icon={faImage} /> Image</th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tbody> {this.state.perfumes.map((perfume) => (
                            <tr>
                                <td>{perfume.perfumeFragranticaId}</td>
                                <td> {perfume.name}</td>
                                <td><FontAwesomeIcon icon={faTransgender} /> {(perfume.gender) }</td>
                                <td> <img src={perfume.image} width="80" height="60" /></td>
                                <td>
                                    <ButtonGroup>
                                        <Link to={"edit/" + perfume.perfumeFragranticaId} className=" btn  btn-outline-primary"><FontAwesomeIcon icon={faEdit} />
                                        </Link>{''}

                                        <Button variant="outline-danger" onClick={this.deletePerfume.bind(this, perfume.perfumeFragranticaId)}><FontAwesomeIcon icon={faTrash} /> </Button>
                                    </ButtonGroup>
                                </td>
                            </tr>
                        ))}

                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        );
    }
}
export default Perfume;