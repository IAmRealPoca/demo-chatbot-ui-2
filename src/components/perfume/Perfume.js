import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Form, Button, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faPlus, faUndo, faList } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
class Perfume extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.initialState;
        this.perfumeChance = this.perfumeChance.bind(this);
        this.submitPerfume = this.submitPerfume.bind(this);
    }
    initialState = {
        perfumeFragranticaId: '',
        name: '',
        votes: '',
        gender: '',
        image: ''
    };
    submitPerfume = event => {
        event.preventDefault();
        const perfume = {
            name: this.state.name,
            votes: this.state.votes,
            gender: this.state.gender,
            image: this.state.image
        };
        axios.post("http://localhost:8686/perfume", perfume)
            .then(res => {
                if (res.data != null) {
                    this.setState(this.initialState);
                    alert("Add succsess");
                }
            })
    };
   
    
        findPerfumeById = (perfumeId) =>{
            axios.put("http://localhost:8686/perfume/"+perfumeId)
            .then(res => {
                if(res.data != null) {
                    this.setState({
                        perfumeFragranticaId: res.data.perfumeFragranticaId, 
                        name: res.data.name,
                        votes: res.data.votes,
                        gender: res.data.gender,
                        image: res.data.image
                    });
                }
            }).catch((error) => {
                console.error("Error- " + error);
            });
    }
    componentDidMount() {
        const perfumeId = +this.props.match.params.id;
        if (perfumeId) {
            this.findPerfumeById(perfumeId);
        }
    }
    resetPerfume = () => {
        this.setState(() => this.initialState);
    };
    perfumeList = () => {
        return this.props.history.push("/perfume/list");
    };

    perfumeChance(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    updatePerfume =(event)=>{
        event.preventDefault();
        const perfume = {
            name: this.state.name,
            votes: this.state.votes,
            gender: this.state.gender,
            image: this.state.image
        };
        axios.put("http://localhost:8686", perfume)
            .then(res => {
                if (res.data != null) {
                    this.setState(this.initialState);
                    alert("Update succsess");
                }
            })
        
    }
    render() {
        const { name, votes, image, gender } = this.state;
        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Card.Header><FontAwesomeIcon icon={faPlus} /> Create Perfume</Card.Header>
                <Form onReset={this.resetPerfume} onSubmit={this.state.id ? this.updatePerfume: this.submitPerfume} id="perfumeFormId">
                    <Card.Body>
                        <Form.Group controlId="formBasicName" >
                            <Form.Label>Name Perfume</Form.Label>
                            <Form.Control required
                                type="text" name="name"
                                value={name}
                                onChange={this.perfumeChance}
                                placeholder="Enter Perfume Name" />
                            <Form.Text className="text-muted">
                                We'll never share your perfume with anyone else.
    </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicVotes">
                            <Form.Label>Votes</Form.Label>
                            <Form.Control required type="number" name="votes" placeholder="1-5"
                                value={votes}
                                onChange={this.perfumeChance} />
                            <Form.Text className="text-muted">
                                Heloo
    </Form.Text>

                            <fieldset>
                                <Form.Group as={Row} controlId="formBasicGender">
                                    <Form.Label as="legend" column sm={2}  >
                                        Gender
      </Form.Label>
                                    <Col sm={10}>
                                        <Form.Check required
                                            type="radio"
                                            label="Female"
                                            value="1"
                                            onChange={this.perfumeChance}
                                            id="formHorizontalRadios1"
                                            name="gender"
                                        />
                                        <Form.Check required
                                            type="radio"
                                            label="Male"
                                            value="2"
                                            id="formHorizontalRadios2"
                                            name="gender"
                                        />
                                        <Form.Check required
                                            type="radio"
                                            label="Gay"
                                            value="3"
                                            id="formHorizontalRadios3"
                                            name="gender"
                                        />
                                    </Col>
                                </Form.Group>
                            </fieldset>
                            <Form>
                                <Form.Group >
                                    <Form.File value={image}
                                        onChange={this.perfumeChance} id="image" label="Image Perfume" name="image" value={image} />
                                </Form.Group>
                            </Form>
                        </Form.Group>
                        <Button variant="success" type="submit"><FontAwesomeIcon icon={faSave} /> {this.state.id ? "Update" :"Submit"}
  </Button>{''}
                        <Button variant="info" type="reset"><FontAwesomeIcon icon={faUndo} />   Reset
  </Button>{''}

                        {/* <Link to="list" className=" text-white"> </Link>{
                             <Button variant="success" type="button"><FontAwesomeIcon icon={faList} />  List Perfume </Button>
                         } */}
                        <Button variant="info" type="button" className=" bg-dark" onClick={this.perfumeList.bind()}>
                            <FontAwesomeIcon icon={faList} />  List Perfume
  </Button>{''}
                    </Card.Body>

                </Form>

            </Card>

        );
    }
}
export default Perfume;