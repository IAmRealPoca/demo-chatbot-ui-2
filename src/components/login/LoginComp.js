
import React from "react";

import axios from "axios";

const requestConfig = {
    headers: {
      "Content-Type": "application/json",
    //   "Access-Control-Allow-Origin": "*",
    },
  };

const Login = () => {
    const [username, setUsername] = React.useState("");

    const [password, setPassword] = React.useState("");

    const loginClickedHandler = () => {
        const loginData = {
            username: username,
            password: password,
        }
        axios.post('http://localhost:8080/api/users/login', loginData, requestConfig)
            .then(response => {
                console.log(response.headers);
                sessionStorage.setItem('token', response.headers.authorization);
            });
    }

    return (
        
        <div>
            
            <p>Username: </p>
            <input type="text"
                onChange={(event) => setUsername(event.target.value)}/>
            <p>Password: </p>
            <input type="password"
                onChange={(event) => setPassword(event.target.value)}/>
            <button onClick={loginClickedHandler}>Login</button>
        </div>
    );
};

export default Login;