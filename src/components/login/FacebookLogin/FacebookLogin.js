import React from "react";
// import {axiosBaseType} from "../../../fetch/AxiosBase";
import axios from "axios";
import FacebookLoginReact from "react-facebook-login";

// const axiosBase = axiosBaseType("auth");

const callback = (response) => {
  console.log(response);
  callServer(response);
}

const callServer = (params) => {
  const requestParam = {
    facebookUserToken: params.accessToken,
    facebookUserId: params.userID,
  };
  axios.post('http://localhost:8080/api/users/login-facebook',requestParam,{headers:{
    // "Access-Control-Allow-Origin":"*"
  }})
  .then(response => {
    console.log(response.data);
  })
  .catch(err => console.log(err));
}


const FacebookLogin = () => {
  return (
    <div>
      {/* <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
    </fb: login - button > */}

      {/* <div class="fb-login-button" 
      data-size="large" 
      data-button-type="continue_with" 
      data-layout="default" 
      data-auto-logout-link="false" 
      data-use-continue-as="false"
      data-width=""
    ></div> */}
    
      <FacebookLoginReact
        appId="261954274914572"
        autoLoad={true}
        fields="name, email, picture"
        callback={callback}
      />

      <div id="status"></div>
    </div >
  );
};

export default FacebookLogin;