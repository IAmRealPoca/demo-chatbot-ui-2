import React from "react";
import "./App.css";

import Navbar from "./components/Navbar/NavbarComp";
import Footer from "./components/Footer/FooterComp";
import BotButton from "./components/chatbot/ChatBotButton/BotButton";

import { Route, Switch, BrowserRouter } from "react-router-dom";

import BookingTable from "./components/booking/ExpertBooking/BookingTable";
import ChatComponent from "./components/newChatbot/ChatComponent";
import Login from "./components/login/LoginComp";
import FacebookLogin from "./components/login/FacebookLogin/FacebookLogin";

import MessengerCustomerChat from "react-messenger-customer-chat";

import Order from './components/Order/Order.jsx';
import Schedule from './components/Schedule/ScheduleComponent.jsx';
import Perfume from './components/perfume/Perfume';
import PerfumeList from './components/perfume/PerfumeList';


// const testCors = () => {
//   console.log("I am running")
//   axios.post("http://localhost:8000/chatbotgreeting/",
//     {
//       "customer": "a",
//       "content": "hi"
//     }
//   ).then(resp => console.log(resp));
//   // axios.get(
//   //   "http://localhost:8099/bookings"
//   // ).then(resp => console.log(resp));
// };


function App() {
  return (
    <div className="App">
      <header>
        <Navbar isLoggedIn="false" />
      </header>
      {/* Header above */}

      <div className="container no-over">
        <div>
          <BrowserRouter>
            <Switch>
              <Route exact path='/chat' component={ChatComponent} />
              <Route exact path='/booking' component={BookingTable} />
              <Route exact path='/login' component={Login} />
              <Route exact path='/order' component={Order}/>
              <Route exact path='/schedule' component={Schedule}/>
              <Route exact path='/perfume/add' component={Perfume}/>
              <Route exact path="/perfume/edit/:id" component={Perfume}/>
              <Route exact path="/perfume/list" component={PerfumeList}/>
            </Switch>
          </BrowserRouter>
          <div>
            {/* <FacebookLogin /> */}
            <MessengerCustomerChat
              pageId="100919221675782"
              appId="559882698225693"
              // htmlRef={window.location.pathname}
            />
          </div>
        </div>
        {/* <BotButton /> */}
        {/* <Button onClick={testCors}>Test cors</Button> */}
      </div>

      {/* Footer below */}
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default App;
