import axios from "axios";

const axiosBase = (type) => {

  let baseEndpoint = "";
  let baseDomain = `${process.env.REACT_APP_BASE_DOMAIN}`;
  if (type === "auth") {
    baseEndpoint = `${process.env.REACT_APP_BASE_AUTH}`;
  } else if (type === "fetch") {
    baseEndpoint = `${process.env.REACT_APP_BASE_API}`;
  }
  if (type === 'chat') {
    baseDomain = `${process.env.REACT_APP_CHATBOTUI_CHAT_DOMAIN}`;
    baseEndpoint = "/chat"
  }
  let instance = axios.create({
    baseURL: `${baseDomain + baseEndpoint}`,
  });
  return instance;
};

// export const axiosBase = instance;
export { axiosBase as axiosBaseType };
